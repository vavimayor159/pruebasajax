json.array!(@entradas) do |entrada|
  json.extract! entrada, :id, :titulo, :contenido
  json.url entrada_url(entrada, format: :json)
end
