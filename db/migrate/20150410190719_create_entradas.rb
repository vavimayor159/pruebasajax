class CreateEntradas < ActiveRecord::Migration
  def change
    create_table :entradas do |t|
      t.string :titulo
      t.text :contenido

      t.timestamps null: false
    end
  end
end
